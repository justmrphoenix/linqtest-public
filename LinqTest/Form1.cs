﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace LinqTest {
    public partial class Form1 : Form {
        static string connectionString = @"Data Source=localhost;Initial Catalog=wsr;Integrated Security=True";
        private DataContext db;
        private Table<User> users;

        public Form1() {
            db = new DataContext(connectionString);
            users = db.GetTable<User>();
            InitializeComponent();
            dataGridView1.DataSource = users;
        }

        private void button1_Click(object sender, EventArgs e) {
            UserForm userForm = new UserForm();
            if(userForm.ShowDialog(this) == DialogResult.OK) {
                User userToAdd = new User();
                userToAdd.Login = userForm.textBox1.Text;
                userToAdd.Password = userForm.textBox2.Text;
                users.InsertOnSubmit(userToAdd);
                db.SubmitChanges();
                dataGridView1.DataSource = users;
                dataGridView1.Refresh();
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            if (dataGridView1.SelectedRows.Count == 0) {
                MessageBox.Show("No row selected!");
                return;
            }
            int index = dataGridView1.SelectedRows[0].Index;
            int id = 0;
            bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
            if (converted == false)
                return;
            UserForm userForm = new UserForm();
            User selectedUser = users.Where(u => u.Id == id).FirstOrDefault();
            userForm.textBox1.Text = selectedUser.Login;
            userForm.textBox2.Text = selectedUser.Password;
            if (userForm.ShowDialog(this) == DialogResult.OK) {
                selectedUser.Login = userForm.textBox1.Text;
                selectedUser.Password = userForm.textBox2.Text;
                db.SubmitChanges();
                dataGridView1.DataSource = users;
                dataGridView1.Refresh();
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            if (dataGridView1.SelectedRows.Count == 0) {
                MessageBox.Show("No row selected!");
                return;
            }
            int index = dataGridView1.SelectedRows[0].Index;
            int id = 0;
            bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
            if (converted == false)
                return;
            UserForm userForm = new UserForm();
            User selectedUser = users.Where(u => u.Id == id).FirstOrDefault();
            users.DeleteOnSubmit(selectedUser);
            db.SubmitChanges();
            dataGridView1.DataSource = users;
            dataGridView1.Refresh();
        }
    }

    [Table(Name = "dbo.Users")]
    public class User {
        [Column(Name = "id", IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column(Name = "login")]
        public string Login { get; set; }
        [Column(Name = "password")]
        public string Password { get; set; }
    }
}
